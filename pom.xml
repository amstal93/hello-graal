<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.gitlab.raonigabriel</groupId>
	<artifactId>hello-graal</artifactId>
	<version>1.0.0</version>
	<description>${project.artifactId}: the classical Java hello-world, compiled to native using GraalVM</description>
	<url>https://gitlab.com/raonigabriel/hello-graal/README.md</url>
	<scm>
		<url>https://gitlab.com/raonigabriel/hello-graal</url>
	</scm>

	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>raonigabriel</id>
			<name>Raoni Gabriel</name>
			<timezone>-3</timezone>
			<url>https://gitlab.com/raonigabriel</url>
			<roles>
				<role>Java Architect</role>
				<role>Cloud Architect</role>
				<role>DevOps Engineer</role>
			</roles>
		</developer>
	</developers>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<mainClass>com.gitlab.raonigabriel.HelloGraal</mainClass>
		<!-- By default, we skip the generation of the binary because it requires 
			GraalVM properly installed -->
		<skipNativeImage>true</skipNativeImage>
	</properties>

	<profiles>
		<!-- This profile will kick-in if it detects a $GRAALVM_HOME env variable -->
		<profile>
			<id>graalvm-installed</id>
			<activation>
				<property>
					<name>env.GRAALVM_HOME</name>
				</property>
			</activation>
			<properties>
				<skipNativeImage>false</skipNativeImage>
			</properties>
		</profile>
	</profiles>

	<build>
		<!-- We enable resource filtering -->
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>

		<plugins>
			<!-- This plugin is used to to generate a runnable, uber-jar and also 
				removes some useless maven meta-data -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<version>3.2.1</version>
				<executions>
					<execution>
						<id>build-jar</id>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<configuration>
							<transformers>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
									<mainClass>${mainClass}</mainClass>
								</transformer>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
							</transformers>
							<filters>
								<filter>
									<artifact>*:*</artifact>
									<excludes>
										<exclude>META-INF/maven/**</exclude>
									</excludes>
								</filter>
							</filters>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<!-- This plugin will create the actual native binary. -->
			<plugin>
				<groupId>com.oracle.substratevm</groupId>
				<artifactId>native-image-maven-plugin</artifactId>
				<version>19.2.1</version>
				<executions>
					<execution>
						<id>build-binary</id>
						<phase>package</phase>
						<goals>
							<goal>native-image</goal>
						</goals>
						<configuration>
							<mainClass>${mainClass}</mainClass>
							<buildArgs>--no-server --static --no-fallback</buildArgs>
							<skip>${skipNativeImage}</skip>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.6.0</version>
				<executions>
					<!-- If we managed to run native-image, we will also run strip, to discard 
						symbols from the binary -->
					<execution>
						<id>strip-binary</id>
						<phase>package</phase>
						<goals>
							<goal>exec</goal>
						</goals>
						<configuration>
							<skip>${skipNativeImage}</skip>
							<executable>strip</executable>
							<workingDirectory>${project.build.directory}</workingDirectory>
							<arguments>
								<argument>${project.artifactId}</argument>
							</arguments>
						</configuration>
					</execution>

					<!-- If we have GraalVM, we will also run this step to reduce the binary 
						size by using upx -->
					<execution>
						<id>upx-binary</id>
						<phase>package</phase>
						<goals>
							<goal>exec</goal>
						</goals>
						<configuration>
							<skip>${skipNativeImage}</skip>
							<executable>upx</executable>
							<workingDirectory>${project.build.directory}</workingDirectory>
							<arguments>
								<argument>--ultra-brute</argument>
								<argument>${project.artifactId}</argument>
							</arguments>
						</configuration>
					</execution>

				</executions>
			</plugin>

			<!-- Finally, we will generate a deb package with the native binary -->
			<plugin>
				<artifactId>jdeb</artifactId>
				<groupId>org.vafer</groupId>
				<version>1.8</version>
				<executions>
					<execution>
						<id>build-deb</id>
						<phase>package</phase>
						<goals>
							<goal>jdeb</goal>
						</goals>
						<configuration>
							<skip>${skipNativeImage}</skip>
							<compression>none</compression>
							<deb>[[buildDir]]/[[artifactId]]_[[version]]_amd64.[[extension]]</deb>
							<dataSet>
								<data>
									<src>${project.build.directory}/${project.artifactId}</src>
									<type>file</type>
									<mapper>
										<type>perm</type>
										<prefix>/usr/bin</prefix>
										<filemode>755</filemode>
									</mapper>
								</data>
							</dataSet>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

</project>